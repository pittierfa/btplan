<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

	protected $fillable = ['name', 'color'];

	public function leaves(){
		return $this->hasMany(Leave::class);
	}

}

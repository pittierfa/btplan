<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
	protected $fillable = ['name', 'date'];

	protected $dates = [
		'created_at',
        'updated_at',
        'date'
	];

}

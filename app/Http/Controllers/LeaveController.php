<?php

namespace App\Http\Controllers;

use App\Http\Resources\LeaveResource;
use App\Leave;
use App\Type;
use App\User;
use Illuminate\Http\Request;

class LeaveController extends Controller {
	public function index() {
		$users = User::all();
		$types = Type::all();

		return view( 'calendar', [ 'users' => $users, 'types' => $types ] );
	}

	public function store( Request $request ) {
		$request->validate( [
			'start'   => 'required|date',
			'end'     => 'required|date',
			'comment' => 'max:255'
		] );
		$leave          = new Leave();
		$leave->user_id = $request->user_id;
		$leave->type_id = $request->type_id;
		$leave->comment = $request->comment;
		$leave->start   = $request->start;
		$leave->end     = $request->end;
		$leave->save();

		return redirect()->route( 'calendar' )->with( 'status', 'Absence ajoutée avec succès' )->with( 'date', $leave->start );
	}

	public function update( Request $request, Leave $leave ) {
		$request->validate( [
			'start'   => 'required|date',
			'end'     => 'required|date',
			'comment' => 'max:255'
		] );
		$leave->user_id = $request->user_id;
		$leave->type_id = $request->type_id;
		$leave->comment = $request->comment;
		$leave->start   = $request->start;
		$leave->end     = $request->end;
		$leave->save();

		return redirect()->route( 'calendar' )->with( 'status', 'Absence modifiée avec succès' )->with( 'date', $leave->start );
	}

	public function delete( Leave $leave ) {
		$startDate = $leave->start;
		$leave->delete();
		return redirect()->route( 'calendar' )->with( 'status', 'Absence supprimée avec succès' )->with( 'date', $startDate );
	}

	public function getAll( Request $request ) {
		if ( ! is_null( $request->start ) && ! is_null( $request->end ) ) {
			return LeaveResource::collection( Leave::whereDate( 'start', '<=', $request->end )->whereDate( 'end', '>=', $request->start )->get() );
		} else {
			return LeaveResource::collection( Leave::all() );
		}
	}
}

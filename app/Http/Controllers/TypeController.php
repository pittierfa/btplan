<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('types.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required|max:255',
			'color' => 'required'
		]);

		$type = new Type();
		$type->name = $request->name;
		$type->color = $request->color;
		$type->save();
		return redirect()->route('config')->with('status', "Type {$request->name} ajouté avec succès");
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Type $type)
	{
		return view('types.edit')->with('type', $type);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Type $type)
	{
		$request->validate([
			'name' => 'required|max:255',
			'color' => 'required'
		]);

		$type->name = $request->name;
		$type->color = $request->color;
		$type->save();

		return redirect()->route('config')->with('status', "Type {$request->name} modifié avec succès");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Type $type)
	{
		$type->delete();
		return redirect()->route('config');
	}
}

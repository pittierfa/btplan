<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LeaveResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
		if ( is_null( $this->comment ) ) {
			$title = $this->user->firstname . ' ' . $this->user->lastname;
		} else {
			$title = $this->user->firstname . ' ' . $this->user->lastname . ' - ' . $this->comment;
		}

		if(!is_null($this->type)){
			$bgc = $this->type->color;
			$bc = $this->type->color;
			$type_id = $this->type->id;
		}else{
			$bgc = "#333";
			$bc = "#333";
			$type_id = null;
			$title = '(SANS TYPE) '. $title;
		}

		return [
			'id'              => $this->id,
			'user_id'         => $this->user->id,
			'type_id'         => $type_id,
			'comment'         => $this->comment,
			'title'           => $title,
			'start'           => $this->start,
			'end'             => date( 'Y-m-d', strtotime( $this->end . ' +1 day' ) ),
			'backgroundColor' => $bgc,
			'borderColor'     => $bc
		];
	}
}

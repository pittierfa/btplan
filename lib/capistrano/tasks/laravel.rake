namespace :laravel do

    desc 'Migrate database'
    task :migrate do
        on roles(:db) do
            within release_path do
                execute :php, :artisan, :migrate, '--force'
            end
        end
    end

    desc 'Setup laravel permissions'
    task :permissions do
        on roles(:web) do
            within release_path do
                execute :chmod, '777', '-R', 'storage'
                execute :chmod, '777', '-R', 'bootstrap/cache'
            end
        end
    end

end
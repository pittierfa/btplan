<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('types')->insert([
    		'name' => 'Vacances',
		    'color' => '#2c3e50'
	    ]);
	    DB::table('types')->insert([
		    'name' => 'Maladie',
		    'color' => '#7f8c8d'
	    ]);
	    DB::table('types')->insert([
		    'name' => 'Formation',
		    'color' => '#8e44ad'
	    ]);
	    DB::table('types')->insert([
		    'name' => 'Piquet',
		    'color' => '#d35400'
	    ]);
	    DB::table('types')->insert([
		    'name' => 'Absences',
		    'color' => '#2980b9'
	    ]);

	    DB::table('holidays')->insert([
		    'name' => 'Noël',
		    'date' => '2017-12-25'
	    ]);

	    DB::table('holidays')->insert([
		    'name' => 'Noël',
		    'date' => '2016-12-25'
	    ]);

	    DB::table('holidays')->insert([
		    'name' => 'Noël',
		    'date' => '2018-12-25'
	    ]);

	    DB::table('holidays')->insert([
		    'name' => 'Nouvel an',
		    'date' => '2018-01-01'
	    ]);
	    DB::table('holidays')->insert([
		    'name' => 'Nouvel an',
		    'date' => '2018-01-02'
	    ]);
    }
}

@include('parts.header')

<div class="mt-3"></div>

@yield('content')

@include('parts.footer')
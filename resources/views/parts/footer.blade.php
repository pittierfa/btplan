<footer class="container text-center mt-4">
    <hr>
    <p>&copy; {{ date('Y') }} Merck KGaA - {{ config('app.name') }} - by Gil Balsiger</p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar-locales.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.fr-CH.min.js') }}"></script>
<script src="{{ asset('js/frappe-charts.min.iife.js') }}"></script>
<script src="{{ asset('js/colpick.js') }}"></script>
<script src="{{ asset('js/app.js') }}?2"></script>
<script src="{{ asset('js/chart.js') }}"></script>
</body>
</html>
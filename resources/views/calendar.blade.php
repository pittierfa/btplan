@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger mt-3" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div id='calendar'></div>
            <div id="color-list">
                <div id="color-list-title">Légende</div>
                <ul>
                    @foreach($types as $type)
                        <li style="background-color: {{ $type->color }};">{{ $type->name }}</li>
                    @endforeach
                </ul>
            </div>
            <hr>
            <h2>Nouvelle absence</h2>
            <form method="post" action="{{ route('leaves.store') }}">
                <div class="form-group">
                    <label for="select_user">Utilisateur</label>
                    <select class="form-control select2" id="select_user" name="user_id" required>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->lastname }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="select_type">Type d'absence</label>
                    <select class="form-control select2" id="select_type" name="type_id" required>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-row input-daterange">
                    <div class="form-group col-md-6">
                        <label for="startDate">Du</label>
                        <input type="text" class="form-control" name="start" id="startDate" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="endDate">Au</label>
                        <input type="text" class="form-control" name="end" id="endDate" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment">Commentaire</label>
                    <input name="comment" id="comment" class="form-control" maxlength="255">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </form>
        </div>

    </main>

    <div class="modal fade" id="updateLeaveModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" id="updateForm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateLeaveModalTitle">Modification de l'absence</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="select_user_update">Utilisateur</label>
                            <select class="form-control select2" id="select_user_update" name="user_id" required>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->lastname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="select_type_update">Type d'absence</label>
                            <select class="form-control select2" id="select_type_update" name="type_id" required>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-row input-daterange">
                            <div class="form-group col-md-6">
                                <label for="startDateUpdate">Du</label>
                                <input type="text" class="form-control" name="start" id="startDateUpdate" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="endDateUpdate">Au</label>
                                <input type="text" class="form-control" name="end" id="endDateUpdate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="commentUpdate">Commentaire</label>
                            <input name="comment" id="commentUpdate" class="form-control" maxlength="255">
                        </div>
                        {{ csrf_field() }}

                    </div>
                    <div class="modal-footer">
                        <a id="deleteBtn" href="#" class="btn btn-danger float-left"
                           onclick="return confirm('Êtes-vous sûr(e) de vouloir supprimer cette absence ?')"><i
                                    class="fa fa-trash"></i></a>
                        <button type="button" role="button" class="btn btn-secondary" data-dismiss="modal">Fermer
                        </button>
                        <button type="submit" role="button" class="btn btn-primary"><i class="fa fa-save"></i> Modifier
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createLeaveModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="{{ route('leaves.store') }}">
                    <div class="modal-header">
                        <h5 class="modal-title">Nouvelle absence</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="select_user_create">Utilisateur</label>
                            <select class="form-control select2" id="select_user_create" name="user_id" required>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->lastname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="select_type_create">Type d'absence</label>
                            <select class="form-control select2" id="select_type_create" name="type_id" required>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-row input-daterange">
                            <div class="form-group col-md-6">
                                <label for="startDateCreate">Du</label>
                                <input type="text" class="form-control" name="start" id="startDateCreate" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="endDateCreate">Au</label>
                                <input type="text" class="form-control" name="end" id="endDateCreate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comment_create">Commentaire</label>
                            <input name="comment" id="comment_create" class="form-control" maxlength="255">
                        </div>
                        {{ csrf_field() }}

                    </div>
                    <div class="modal-footer">
                        <button type="button" role="button" class="btn btn-secondary" data-dismiss="modal">Fermer
                        </button>
                        <button type="submit" role="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <span id="dateUpdated">{{ session('date') }}</span>
@endsection
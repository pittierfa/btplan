@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            <h1>Modification de {{ $user->firstname }} {{ $user->lastname }}</h1>
            <hr>
            <form method="post" action="{{ route('users.update', $user->id) }}">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="firstname">Prénom</label>
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Prénom du l'utilisateur" value="{{ $user->firstname }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lastname">Nom</label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Nom de l'utilisateur" value="{{ $user->lastname }}" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="muid">MUID / XUID</label>
                        <input type="text" class="form-control" name="muid" id="muid" placeholder="M/X de l'utilisateur" value="{{ $user->muid }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail de l'utilisateur" value="{{ $user->email }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">Adresse</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Adresse de l'utilisateur" value="{{ $user->address }}">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="birth">Anniversaire</label>
                        <input type="text" class="form-control datepicker" name="birth" id="birth" value="{{ !is_null($user->birth) ? $user->birth->format('Y-m-d') : '' }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone">N° de téléphone</label>
                        <div class="input-group">
                            <span class="input-group-addon">+41</span>
                            <input type="tel" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <button type="submit" class="btn btn-primary">Modifier</button>
                <a href="{{ route('users.index') }}" class="btn btn-secondary">Annuler</a>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger mt-3" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    </main>
@endsection

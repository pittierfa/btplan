@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container-fluid">
            <h1 style="display: inline-block">Utilisateurs</h1>
            <a href="{{ route('users.create') }}" class="btn btn-success"
               style="display: inline-block;vertical-align: top;"><i class="fa fa-user-plus"></i></a>
            <hr>
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <table class="table">
                <tr>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>MUID</th>
                    <th>E-mail</th>
                    <th>Anniversaire</th>
                    <th>Adresse</th>
                    <th>Téléphone</th>
                    <th>Actions</th>
                </tr>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->firstname }}</td>
                        <td>{{ $user->lastname }}</td>
                        <td>{{ $user->muid }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ !is_null($user->birth) ? $user->birth->format('d M') : '' }}</td>
                        <td>{{ $user->address }}</td>
                        <td>{{ !is_null($user->phone) ? '0'.$user->phone : '' }}</td>
                        <td>
                            <a href="{{ route('users.edit', $user->id) }}"
                               class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            <form method="post" action="{{ route('users.destroy', $user->id) }}"
                                  style="display: inline;">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" style="cursor: pointer;"
                                        onclick="return confirm('Êtes-vous sûr(e) de vouloir supprimer cet utilisateur ?')">
                                    <i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </main>
@endsection
